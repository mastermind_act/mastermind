import requests as rq
import json

def load_words_from_file(file_path):   
    with open(file_path, 'r') as file:
        words = [line.strip() for line in file if len(set(line.strip())) == 5]
    return words

def create_id():
    global session

    mm_url = 'https://we6.talentsprint.com/wordle/game/'
    register = mm_url + 'register'
    register_dict = {"mode": "mastermind", "name": "masterbot"}
    register_with = json.dumps(register_dict)

    r = session.post(register, json=register_dict)
    me = r.json()['id']

    creat_url = mm_url + 'create'
    creat_dict = {'id': me, 'overwrite': True}
    rc = session.post(creat_url, json=creat_dict)

    return me

def send_guess(word: str, me: str) -> dict:
    global session

    mm_url = 'https://we6.talentsprint.com/wordle/game/'
    guess_url = mm_url + 'guess'
    guess = {'id': me, 'guess': word}
    correct = session.post(guess_url, json=guess)
    return correct.json()

def update_data(guess: str, feedback: int, word_dr: list[str]) -> list[str]:
    return [word for word in word_dr if len(set(word.strip()) & set(guess.strip())) == feedback and word != guess]

def run_game():
    
    id = create_id()
    file_path = '5letters.txt'
    word_dr = load_words_from_file(file_path)

    guess = word_dr[0]
    outcome = send_guess(guess, id)
    
    while 'win' not in outcome['message'] :
        feedback = outcome['feedback']
        print(f"Guess: {guess}, Feedback: {feedback}, {outcome['message']}")
        
        word_dr = update_data(guess, feedback, word_dr)
        guess = word_dr[0]
        outcome = send_guess(guess, id)

    print(f"{outcome['message']} The correct word is {guess}.")


session = rq.Session()

run_game()
